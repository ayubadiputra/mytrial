<?php

/**
 * Single post display
 *
 * @package My Trial
 * @since My Trial 1.0.0
 */

get_header();

while ( have_posts() ) : the_post(); ?>

<section class="content">
    <div class="row">
        <div id="mainbar" class="col-md-12">
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>


<?php endwhile; ?>

<?php get_footer(); ?>