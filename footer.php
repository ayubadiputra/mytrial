<?php

/**
 * The template for displaying the footer
 *
 * Displays all of the head element.
 *
 * @package My Trial
 * @since My Trial 1.0.0
 */

?>
            <footer class="footer-area">
                <p><?php printf( __( 'Proudly powered by %s', MYT_DOMAIN ), 'WordPress' ); ?></p>
            </footer>

        </div>
    </section>
    <?php wp_footer(); ?>
    </body>
</html>