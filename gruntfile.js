module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            // this is the "dev" Sass config used with "grunt watch" command
            dev: {
                options: {
                    style: 'expanded',
                    // tell Sass to look in the Bootstrap stylesheets directory when compiling
                    loadPath: 'node_modules/bootstrap-sass/assets/stylesheets'
                },
                files: {
                    // the first path is the output and the second is the input
                    'assets/css/mytrial.css': 'assets/scss/mytrial.scss'
                }
            },
            // this is the "production" Sass config used with the "grunt buildcss" command
            dist: {
                options: {
                    style: 'compressed',
                    loadPath: 'node_modules/bootstrap-sass/assets/stylesheets'
                },
                files: {
                    'assets/css/mytrial.css': 'assets/scss/mytrial.scss'
                }
            }
        },
        // configure the "grunt watch" task
        watch: {
            sass: {
                files: 'scss/*.scss',
                tasks: ['sass:dev']
            }
        },
        // configure the "grunt copy" task
        copy: {
            main: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js'],
                    dest: 'assets/js/',
                    filter: 'isFile'
                }],
            },
        },
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // "grunt buildcss" is the same as running "grunt sass:dist".
    // if I had other tasks, I could add them to this array.
    grunt.registerTask( 'default', ['sass:dist', 'copy:main']);
};