<?php

/**
 * My Trial functions and definitions
 *
 * @package My Trial
 * @since 1.0.0
 */

if ( ! function_exists( 'myt_setup' ) ) {

    /**
     * Setup Borobudur Village theme
     */
    function myt_setup() {

        // Main setup
        load_theme_textdomain( 'mytrial' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );

        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        add_theme_support( 'customize-selective-refresh-widgets' );

        // Need research
        add_theme_support( 'myt-logo', array(
            'height'      => 240,
            'width'       => 240,
            'flex-height' => true,
        ) );

        set_post_thumbnail_size( 1200, 9999 );

    }

}
add_action( 'after_setup_theme', 'myt_setup' );

if ( ! function_exists( 'myt_set_constant' ) ) {

    /**
     * Set theme constant
     */
    function myt_set_constant() {
        defined( 'MYT_DOMAIN' )  OR define( 'MYT_DOMAIN', 'mytrial' );
        defined( 'MYT_DIR' )     OR define( 'MYT_DIR',    get_template_directory() );
        defined( 'MYT_URI' )     OR define( 'MYT_URI',    get_template_directory_uri() );
    }

}
add_action( 'init', 'myt_set_constant' );

if ( ! function_exists( 'myt_widget' ) ) {

    /**
     * Register sidebar settings
     */
    function myt_widget() {

        register_sidebar( array(
            'name'          => __( 'Main Sidebar', 'mytrial' ),
            'id'            => 'main-sidebar',
            'description'   => __( 'Display widget in main sidebar.', 'mytrial' ),
            'before_widget' => '<aside id="%1$s" class="widget custom_class %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );

    }

}
add_action( 'widgets_init', 'myt_widget' );

if ( ! function_exists( 'myt_scipts' ) ) {

    /**
     * Enqueue stylesheet and script
     */
    function myt_scripts() {

        // Theme stylesheet.
        // wp_enqueue_style( 'myt-import', MYT_URI . '/assets/css/imports.css', array(), '1.0.0' );
        wp_enqueue_style( 'myt-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700', array(), '1.0.0' );
        wp_enqueue_style( 'myt-style', get_stylesheet_uri() );

        // Theme scripts
        wp_enqueue_script( 'myt-bootstrap', MYT_URI . '/assets/js/bootstrap.min.js', array( 'jquery' ), '1.0.0' );

    }

}
add_action( 'wp_enqueue_scripts', 'myt_scripts' );

if ( ! function_exists( 'myt_includes' ) ) {

    /**
     * Includes all required files
     */
    function myt_includes() {
        // require_once MYT_DIR . '/inc/actions.php';
        require_once MYT_DIR . '/inc/filters.php';
        // require_once MYT_DIR . '/inc/helpers.php';
        // require_once MYT_DIR . '/inc/libs/wp_bootstrap_navwalker.php';
        // require_once MYT_DIR . '/inc/widgets.php';
    }

}
add_action( 'init', 'myt_includes' );

if ( ! function_exists( 'myt_register_components' ) ) {

    /**
     * Register additional components
     */
    function myt_register_components() {

        register_nav_menus( array(
            'main_menu'         => 'Main Menu',
        ) );

    }

}
add_action( 'init', 'myt_register_components' );