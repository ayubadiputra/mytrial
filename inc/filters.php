<?php

/**
 * All filters in mytrial
 *
 * @package My Trial
 * @since My Trial 1.0.0
 */

if ( ! function_exists( 'myt_change_stylesheet' ) ) {

    /**
     * Override style.css with custom CSS as main style.
     * @param  string $style Current main CSS file
     * @return string        Path to custom main CSS file
     */
    function myt_change_stylesheet( $style ) {
        return MYT_URI . '/assets/css/mytrial.css';
    }

}
add_filter( 'stylesheet_uri', 'myt_change_stylesheet' );