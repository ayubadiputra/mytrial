<?php

/**
 * Main display
 *
 * Displays all of the head element.
 *
 * @package My Trial
 * @since My Trial 1.0.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
<div class="content-area">
    <div class="row">
        <div id="post-container" class="col-md-12">
            <?php while ( have_posts() ) : the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class( 'post-list' ); ?>>
                <h2 class="mst-title">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h2>
                <div class="mst-meta">
                    <span class="mst-meta-item mst-user">
                        <label>Taken by:</label> <?php echo get_the_author(); ?>
                    </span>
                    <span class="mst-meta-item mst-value">
                        <?php echo esc_html( get_post_meta ( get_the_ID(), 'mst_test_taken_skill_value', true ) ); ?>%
                    </span>
                    <span class="mst-meta-item mst-duration">
                        <?php echo esc_html( get_post_meta ( get_the_ID(), 'mst_test_taken_time_display', true ) ); ?>
                    </span>
                    <span class="mst-meta-item mst-status">
                        <?php echo esc_html( get_post_meta ( get_the_ID(), 'mst_test_status', true ) ); ?>
                    </span>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php get_footer(); ?>